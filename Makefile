slides.pdf: slides.md *.hs
	pandoc \
		-t beamer \
		--filter pandoc-include-code \
		--listings \
		slides.md \
		-o slides.pdf
