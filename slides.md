---
title: Servant
subtitle: Introduction and Hacking Session
author: Oskar Wickstrom
date: March 8, 2017
header-includes:
- \usepackage[utf8]{inputenc}
- \usepackage{listingsutf8}
- \usepackage[T1]{fontenc}
- \usepackage[sfdefault]{FiraSans}
- \usepackage{newtxsf}
- \usepackage{FiraMono}
- \usepackage{xcolor}
- \lstset{inputencoding=utf8,basicstyle=\ttfamily\small,keywordstyle=\color{blue}\ttfamily,stringstyle=\color{purple}\ttfamily,commentstyle=\color{gray}\ttfamily}
---

# Type-Level Routing

## Why?

* Representing web routes at type-level provides more safety
* But also a lot of stuff for free!
    - Automatically derived clients
    - Automatic encoding/decoding
    - Automatic links, at compile-time
    - Generating client types and accessor code

## How?

* Represent web application routes as types
* Provide handler functions matching those types
* Combine them as a server

## Minimal Routing

* `/` route
    - responds to `GET` requests
    - responds with HTML

## Minimal Route Type

```haskell
data IndexResource = IndexResource

type MySite = Get '[HTML] IndexResource
```

## Literal Path Segments

Let's say we want `/index` instead...

## Minimal Route Type

```haskell
data IndexResource = IndexResource

type MySite =
  "index" :> Get '[HTML] IndexResource
```

## Multiple Endpoints

But we want multiple endpoints!

## Multiple Endpoints Type

```haskell
data IndexResource = IndexResource
data AboutResource = AboutResource

type MySite =
       "index" :> Get '[HTML] IndexResource
  :<|> "about" :> Get '[HTML] AboutResource
```

# The Meetup API&trade;

## Loose Specification

* `/meetups`
    - responds to `GET` requests
    - has a `host` query parameter
    - returns a JSON array of upcoming meetups (filtered by host, if provided)

* `/meetups/:id`
    - responds to `GET` requests
    - returns a specific meetup by ID, or 404 if not existing

## Our Datatypes

```{.haskell include=Main.hs snippet=datatypes}
```

## Route Type

```{.haskell include=Main.hs snippet=routing-type}
```

# Handlers

## Our Database

```{.haskell include=Main.hs snippet=database}
```

## Filtered Meetups

```{.haskell include=Main.hs snippet=meetups}
```

## Find a Meetup

```{.haskell include=Main.hs snippet=findMeetup}
```

# Server

## Creating a Server

```{.haskell include=Main.hs snippet=server}
```

## Running the Server

```{.haskell include=Main.hs snippet=main}
```

## What do we get?

```{.shell basicstyle=\ttfamily\scriptsize}
$ curl -s http://localhost:3000/meetups | jq .
[
  {
    "topics": [
      "Hyper",
      "Halogen"
    ],
    "meetupId": 1,
    "host": "Jayway"
  },
  ...
]
```

## With a Filter

```{.shell basicstyle=\ttfamily\scriptsize}
$ curl -s http://localhost:3000/meetups?host=Sony | jq .
[
  {
    "topics": [
      "Haskell",
      "Servant"
    ],
    "meetupId": 3,
    "host": "Sony"
  }
]
```

## Find a Specific Meetup

```{.shell basicstyle=\ttfamily\scriptsize}
$ curl -s http://localhost:3000/meetups/2 | jq .
{
  "topics": [
    "Reason",
    "Lens"
  ],
  "meetupId": 2,
  "host": "Jayway"
}
```

## 404

```{.shell basicstyle=\ttfamily\scriptsize}
$ curl -i http://localhost:3000/meetups/4
HTTP/1.1 404 Not Found
Transfer-Encoding: chunked
Date: Wed, 08 Mar 2017 06:39:17 GMT
Server: Warp/3.2.11
```

# Time for hacking!

## TODO

* We could build:
    - More features in the Meetup API&trade;
    - A web server with HTML and JSON content types
    - A client for some existing REST API (e.g. SWAPI)
    - Client + server with Elm or PureScript
* Split in groups?
