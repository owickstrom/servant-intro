# Servant - Introduction & Hacking Session

A short introduction to Servant, and the starting point for a hacking session.

## Make Slides

    make

## Run Server

    stack setup
    stack build
    stack exec servant-intro
