{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase    #-}
{-# LANGUAGE TypeOperators #-}
module Main where

import           Data.Aeson
import qualified Data.List                as List
import           GHC.Generics
import           Network.Wai.Handler.Warp
import           Servant

-- start snippet datatypes
type Host = String
type Topic = String

data Meetup
  = Meetup { meetupId :: Int
           , host     :: Host
           , topics   :: [Topic]
           } deriving (Show, Eq, Ord, Generic)

instance ToJSON Meetup
-- end snippet datatypes

-- start snippet routing-type
type MeetupAPI =
       "meetups" :> QueryParam "host" Host
                 :> Get '[JSON] [Meetup]
  :<|> "meetups" :> Capture "id" Int
                 :> Get '[JSON] Meetup
-- end snippet routing-type

-- start snippet database
allMeetups :: [Meetup]
allMeetups =
  [ Meetup 1 "Jayway" ["Hyper", "Halogen"]
  , Meetup 2 "Jayway" ["Reason", "Lens"]
  , Meetup 3 "Sony" ["Haskell", "Servant"]
  ]
-- end snippet database

-- start snippet meetups
meetups :: Maybe Host -> Handler [Meetup]
meetups =
  \case
    Just wanted ->
      return matching
      where
        matching =
          List.filter
          ((==) wanted . host)
          allMeetups
    Nothing ->
      return allMeetups
-- end snippet meetups

-- start snippet findMeetup
findMeetup :: Int -> Handler Meetup
findMeetup id' =
  case found of
    Just meetup -> return meetup
    Nothing     -> throwError err404
  where
    found =
      List.find
      ((==) id' . meetupId)
      allMeetups
-- end snippet findMeetup

-- start snippet server
server :: Server MeetupAPI
server = meetups :<|> findMeetup
-- end snippet server

-- start snippet main
meetupAPI :: Proxy MeetupAPI
meetupAPI = Proxy

app :: Application
app = serve meetupAPI server

main :: IO ()
main = run 3000 app
-- end snippet main
